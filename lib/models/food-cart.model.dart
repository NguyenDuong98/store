import 'package:banhistore/models/food.model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'food-cart.model.g.dart';

@JsonSerializable(includeIfNull: true)
class FoodCartModel {
  String objectId;
  int amount;
  FoodModel food;

  FoodCartModel({this.objectId, this.amount, this.food});

  factory FoodCartModel.fromJson(Map<String, dynamic> json) =>
      _$FoodCartModelFromJson(json);
  Map<String, dynamic> toJson() => _$FoodCartModelToJson(this);
}
