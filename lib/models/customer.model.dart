import 'package:json_annotation/json_annotation.dart';
part 'customer.model.g.dart';

@JsonSerializable(includeIfNull: true)
class CustomerModel {
  String objectId;
  String customer;
  String refund;

  CustomerModel({this.objectId, this.customer, this.refund});

  factory CustomerModel.fromJson(Map<String, dynamic> json) =>
      _$CustomerModelFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);
}
