// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refund.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefundModel _$RefundModelFromJson(Map<String, dynamic> json) {
  return RefundModel(
      objectId: json['objectId'] as String,
      grocery: json['grocery'] as String,
      fruit: json['fruit'] as String,
      food: json['food'] as String);
}

Map<String, dynamic> _$RefundModelToJson(RefundModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'grocery': instance.grocery,
      'fruit': instance.fruit,
      'food': instance.food
    };
