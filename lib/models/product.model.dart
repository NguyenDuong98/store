import 'package:json_annotation/json_annotation.dart';
part 'product.model.g.dart';

@JsonSerializable(includeIfNull: true)
class ProductModel {
  String objectId;
  String productName;
  String cost;
  String price;
  String supplier;
  String amount;
  String imageProduct;

  ProductModel({
    this.objectId,
    this.cost,
    this.price,
    this.productName,
    this.supplier,
    this.amount,
    this.imageProduct,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);
  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
