import 'package:json_annotation/json_annotation.dart';
part 'food.model.g.dart';

@JsonSerializable(includeIfNull: true)
class FoodModel {
  String objectId;
  String nameFood;
  String cost;
  String price;
  bool status;
  String imageFood;

  FoodModel({
    this.objectId,
    this.nameFood,
    this.cost,
    this.price,
    this.status,
    this.imageFood,
  });

  factory FoodModel.fromJson(Map<String, dynamic> json) =>
      _$FoodModelFromJson(json);
  Map<String, dynamic> toJson() => _$FoodModelToJson(this);
}
