// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerModel _$CustomerModelFromJson(Map<String, dynamic> json) {
  return CustomerModel(
    objectId: json['objectId'] as String,
    customer: json['customer'] as String,
    refund: json['refund'] as String,
  );
}

Map<String, dynamic> _$CustomerModelToJson(CustomerModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'customer': instance.customer,
      'refund': instance.refund
    };
