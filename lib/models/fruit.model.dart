import 'package:json_annotation/json_annotation.dart';
part 'fruit.model.g.dart';

@JsonSerializable(includeIfNull: true)
class FruitModel {
  String objectId;
  String fruitName;
  String origin;
  String cost;
  String price;
  bool status;
  String imageFruit;

  FruitModel({
    this.objectId,
    this.cost,
    this.price,
    this.fruitName,
    this.status,
    this.origin,
    this.imageFruit,
  });

  factory FruitModel.fromJson(Map<String, dynamic> json) =>
      _$FruitModelFromJson(json);
  Map<String, dynamic> toJson() => _$FruitModelToJson(this);
}
