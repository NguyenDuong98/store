// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'food-cart.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FoodCartModel _$FoodCartModelFromJson(Map<String, dynamic> json) {
  return FoodCartModel(
    objectId: json['objectId'] as String,
    amount: json['amount'] as int,
    food: json['food'] as FoodModel,
  );
}

Map<String, dynamic> _$FoodCartModelToJson(FoodCartModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'amount': instance.amount,
      'food': instance.food
    };
