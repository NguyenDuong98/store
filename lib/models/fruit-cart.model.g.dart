// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fruit-cart.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FruitCartModel _$FruitCartModelFromJson(Map<String, dynamic> json) {
  return FruitCartModel(
    objectId: json['objectId'] as String,
    amount: json['amount'] as int,
    fruit: json['fruit'] as FruitModel,
  );
}

Map<String, dynamic> _$FruitCartModelToJson(FruitCartModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'amount': instance.amount,
      'fruit': instance.fruit
    };
