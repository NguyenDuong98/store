// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'food.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FoodModel _$FoodModelFromJson(Map<String, dynamic> json) {
  return FoodModel(
      objectId: json['objectId'] as String,
      nameFood: json['nameFood'] as String,
      cost: json['cost'] as String,
      price: json['price'] as String,
      status: json['status'] as bool,
      imageFood: json['imageFood']['url'] as String);
}

Map<String, dynamic> _$FoodModelToJson(FoodModel instance) => <String, dynamic>{
      'objectId': instance.objectId,
      'nameFood': instance.nameFood,
      'cost': instance.cost,
      'price': instance.price,
      'status': instance.status,
      'imageFood': instance.imageFood
    };
