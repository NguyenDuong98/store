// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product-cart.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductCartModel _$ProductCartModelFromJson(Map<String, dynamic> json) {
  return ProductCartModel(
    objectId: json['objectId'] as String,
    amount: json['amount'] as int,
    product: json['product'] as ProductModel,
  );
}

Map<String, dynamic> _$ProductCartModelToJson(ProductCartModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'amount': instance.amount,
      'product': instance.product
    };
