import 'package:banhistore/models/fruit.model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'fruit-cart.model.g.dart';

@JsonSerializable(includeIfNull: true)
class FruitCartModel {
  String objectId;
  int amount;
  FruitModel fruit;

  FruitCartModel({this.objectId, this.amount, this.fruit});

  factory FruitCartModel.fromJson(Map<String, dynamic> json) =>
      _$FruitCartModelFromJson(json);
  Map<String, dynamic> toJson() => _$FruitCartModelToJson(this);
}
