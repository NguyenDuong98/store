// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fruit.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FruitModel _$FruitModelFromJson(Map<String, dynamic> json) {
  return FruitModel(
      objectId: json['objectId'] as String,
      fruitName: json['fruitName'] as String,
      cost: json['cost'] as String,
      price: json['price'] as String,
      status: json['status'] as bool,
      origin: json['origin'] as String,
      imageFruit: json['imageFruit']['url'] as String);
}

Map<String, dynamic> _$FruitModelToJson(FruitModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'fruitName': instance.fruitName,
      'cost': instance.cost,
      'price': instance.price,
      'status': instance.status,
      'origin': instance.origin,
      'imageFruit': instance.imageFruit
    };
