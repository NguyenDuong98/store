// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) {
  return ProductModel(
      objectId: json['objectId'] as String,
      productName: json['productName'] as String,
      cost: json['cost'] as String,
      price: json['price'] as String,
      supplier: json['supplier'] as String,
      amount: json['amount'] as String,
      imageProduct: json['imageProduct']['url'] as String);
}

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'productName': instance.productName,
      'cost': instance.cost,
      'price': instance.price,
      'supplier': instance.supplier,
      'amount': instance.amount,
      'imageProduct': instance.imageProduct
    };
