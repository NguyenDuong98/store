import 'package:banhistore/models/product.model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'product-cart.model.g.dart';

@JsonSerializable(includeIfNull: true)
class ProductCartModel {
  String objectId;
  int amount;
  ProductModel product;

  ProductCartModel({this.objectId, this.amount, this.product});

  factory ProductCartModel.fromJson(Map<String, dynamic> json) =>
      _$ProductCartModelFromJson(json);
  Map<String, dynamic> toJson() => _$ProductCartModelToJson(this);
}
