import 'package:json_annotation/json_annotation.dart';
part 'refund.model.g.dart';

@JsonSerializable(includeIfNull: true)
class RefundModel {
  String objectId;
  String grocery;
  String fruit;
  String food;

  RefundModel({
    this.objectId,
    this.grocery,
    this.fruit,
    this.food,
  });

  factory RefundModel.fromJson(Map<String, dynamic> json) =>
      _$RefundModelFromJson(json);
  Map<String, dynamic> toJson() => _$RefundModelToJson(this);
}
