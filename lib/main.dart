import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:banhistore/views/tab/tab.view.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final keyApplicationId = 'jmhJqHxZ2IGU9A4ABWsqHhjG55Dh4TGKOgjKY0kX';
  final keyClientKey = '4xzxgscSyAibv0yN59bOlhLDHcLdkUkbBhPZQBZD';
  final keyParseServerUrl = 'https://parseapi.back4app.com';
  await Parse().initialize(keyApplicationId, keyParseServerUrl,
      clientKey: keyClientKey, debug: true);

  runApp(GetMaterialApp(home: Tabs()));
}
