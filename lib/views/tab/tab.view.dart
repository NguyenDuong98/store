import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_icons/flutter_icons.dart';

import 'package:banhistore/utility/cart.utility.dart';
import 'package:banhistore/views/food/food.view.dart';
import 'package:banhistore/views/fruits/fruits.view.dart';
import 'package:banhistore/views/grocery/grocery.view.dart';
import 'package:banhistore/views/pay/cart.view.dart';
import 'package:banhistore/views/store/store.view.dart';

class Tabs extends StatefulWidget {
  Tabs({Key key}) : super(key: key);

  @override
  TabsState createState() => TabsState();
}

class TabsState extends State<Tabs> {
  bool order;
  int totalProduct;
  int _selectedIndex = 0;
  List<Widget> _widgetOptions;
  TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  final cartutility = CartUtility();

  _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  _addToCart() {
    setState(() {
      order = true;
      totalProduct = cartutility.getToTalProduct();
    });
  }

  @override
  void initState() {
    super.initState();
    totalProduct = cartutility.getToTalProduct();
    _widgetOptions = <Widget>[
      Grocery(addToCart: _addToCart),
      Fruits(addToCart: _addToCart),
      Food(addToCart: _addToCart),
      Store()
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            showDialog(
                context: context,
                builder: (_) => StatefulBuilder(builder: (context, setState) {
                      return Cart();
                    })).then((value) => {
                  setState(() => totalProduct = cartutility.getToTalProduct()),
                  FocusScope.of(context).requestFocus(new FocusNode())
                });
          },
          child: Container(
              width: double.infinity,
              height: double.infinity,
              child: totalProduct == 0
                  ? Icon(FontAwesome.cart_plus)
                  : Stack(alignment: Alignment.center, children: [
                      Positioned(
                          child: Container(
                              margin: EdgeInsets.only(right: 10, top: 10),
                              child: Icon(FontAwesome.cart_plus))),
                      Positioned(
                          child: Container(
                              width: 25,
                              height: 25,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 15, bottom: 15),
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(100)),
                              child: Text(totalProduct.toString(),
                                  style: TextStyle(fontSize: 13))))
                    ])),
          backgroundColor: Colors.cyan[500]),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: _selectedIndex == 0
                ? Image.asset('assets/images/grocery-active.png')
                : Image.asset('assets/images/grocery-active.png'),
            label: 'Tạp Hóa',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 1
                ? Image.asset('assets/images/fruit_active.png')
                : Image.asset('assets/images/fruit_active.png'),
            label: 'Trái Cây',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 2
                ? Image.asset('assets/images/drink-active.png')
                : Image.asset('assets/images/drink-active.png'),
            label: 'Đồ Ăn',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 3
                ? Image.asset('assets/images/store-active.png')
                : Image.asset('assets/images/store-active.png'),
            label: 'Cửa Hàng',
          )
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: _onItemTapped,
      ),
    );
  }
}
