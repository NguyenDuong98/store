import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/models/refund.model.dart';
import 'package:banhistore/utility/general.utility.dart';

class Refund extends StatefulWidget {
  Refund({Key key}) : super(key: key);

  @override
  _RefundState createState() => _RefundState();
}

class _RefundState extends State<Refund> {
  String grocery = '';
  String fruit = '';
  String food = '';
  RefundModel refund;
  GeneralUtility generalUtility = GeneralUtility();

  var txtGroceryTextField = TextEditingController();
  var txtFruitTextField = TextEditingController();
  var txtFoodTextField = TextEditingController();
  final storeController = Get.put(StoreController());

  _getRefund() {
    storeController.getRefund().then((value) => setState(() {
          refund = value[0];
          txtGroceryTextField.text = value[0].grocery;
          txtFruitTextField.text = value[0].fruit;
          txtFoodTextField.text = value[0].food;
        }));
  }

  _updateRefund() async {
    RefundModel newRefund = RefundModel(
        grocery: grocery == '' ? refund.grocery : grocery,
        fruit: fruit == '' ? refund.fruit : fruit,
        food: food == '' ? refund.food : food);
    final response =
        await storeController.updateRefund(refund.objectId, newRefund);
    if (response != null) {
      Navigator.of(context).pop();
      generalUtility.showNotification('Cập Nhật Hoàn Tiền Thành Công');
    } else {
      generalUtility.showNotification('Đã Xãy Ra Lỗi Cập Nhật Hoàn Tiền');
    }
  }

  @override
  void initState() {
    super.initState();
    _getRefund();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Hoàn Tiền'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(child: Text('Hoàn Tiền Tạp Hóa')),
                          Container(
                            height: 50,
                            child: TextField(
                              controller: txtGroceryTextField,
                              onChanged: (value) {
                                grocery = value;
                              },
                              decoration: new InputDecoration(
                                  suffixText: '%',
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.greenAccent,
                                          width: 1))),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text('Hoàn Tiền Trái Cây')),
                          Container(
                            height: 50,
                            child: TextField(
                              controller: txtFruitTextField,
                              onChanged: (value) {
                                fruit = value;
                              },
                              decoration: new InputDecoration(
                                  suffixText: '%',
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.greenAccent,
                                          width: 1))),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text('Hoàn Tiền Ăn Vặt')),
                          Container(
                            height: 50,
                            child: TextField(
                              controller: txtFoodTextField,
                              onChanged: (value) {
                                food = value;
                              },
                              decoration: new InputDecoration(
                                  suffixText: '%',
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.greenAccent,
                                          width: 1))),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          InkWell(
                              onTap: () {
                                _updateRefund();
                              },
                              child: Container(
                                  height: 50,
                                  width: width,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(top: 15),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.blue[600].withOpacity(0.8)),
                                  child: Text('Cập Nhật',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold))))
                        ])))));
  }
}
