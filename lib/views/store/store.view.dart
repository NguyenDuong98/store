import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:banhistore/views/store/import-grocery/search-product.view.dart';
import 'package:banhistore/views/store/import-fruit/search-fruit.view.dart';
import 'package:banhistore/views/store/import-food/search-food.view.dart';
import 'package:banhistore/views/store/refund/refund.view.dart';

class Store extends StatefulWidget {
  Store({Key key}) : super(key: key);

  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxScrolled) {
                  return <Widget>[createSilverAppBar1()];
                },
                body: Container(
                  color: Colors.grey[200],
                  child: SingleChildScrollView(
                      child: Column(children: [
                    Row(children: [
                      Expanded(
                          child: Container(
                        child: InkWell(
                            onTap: () {},
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, left: 10, right: 5),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      flex: 6,
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/customer.png'))))),
                                  Expanded(
                                      flex: 4,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 15),
                                        child: Text('Khách Hàng',
                                            style: TextStyle(fontSize: 20)),
                                      ))
                                ]))),
                      )),
                      Expanded(
                        child: InkWell(
                            onTap: () {},
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, left: 5, right: 10),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/business-and-finance.png'))))),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.only(top: 15),
                                    child: Text('Doanh Thu',
                                        style: TextStyle(fontSize: 20)),
                                  ))
                                ]))),
                      )
                    ]),
                    Row(children: [
                      Expanded(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Refund()),
                              );
                            },
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, right: 5, left: 10),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/refund.png'))))),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.only(top: 15),
                                    child: Text('Hoàn Tiền',
                                        style: TextStyle(fontSize: 20)),
                                  ))
                                ]))),
                      ),
                      Expanded(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchProduct()),
                              );
                            },
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, right: 10, left: 5),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/import.png'))))),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.only(top: 15),
                                    child: Text('Nhập Hàng',
                                        style: TextStyle(fontSize: 20)),
                                  ))
                                ]))),
                      )
                    ]),
                    Row(children: [
                      Expanded(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchFruit()),
                              );
                            },
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, right: 5, left: 10),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/fruits.png'))))),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.only(top: 15),
                                    child: Text('Trái Cây',
                                        style: TextStyle(fontSize: 20)),
                                  ))
                                ]))),
                      ),
                      Expanded(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchFood()),
                              );
                            },
                            child: Container(
                                height: 120,
                                margin: EdgeInsets.only(
                                    top: 10, right: 10, left: 5),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(children: [
                                  Expanded(
                                      child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/fast-food.png'))))),
                                  Expanded(
                                      child: Container(
                                    margin: EdgeInsets.only(top: 15),
                                    child: Text('Ăn Vặt',
                                        style: TextStyle(fontSize: 20)),
                                  ))
                                ]))),
                      )
                    ])
                  ])),
                ))));
  }

  SliverAppBar createSilverAppBar1() {
    return SliverAppBar(
      backgroundColor: Colors.redAccent.withOpacity(0),
      expandedHeight: 150,
      floating: false,
      elevation: 0,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: Container(
            color: Colors.white,
            child: Image.asset(
              'assets/images/banner.png',
              fit: BoxFit.cover,
            ),
          ),
        );
      }),
    );
  }
}
