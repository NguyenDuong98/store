import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:tiengviet/tiengviet.dart';
import 'package:get/get.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/views/share/foods.view.dart';
import 'package:banhistore/views/store/import-food/import.view.dart';

class SearchFood extends StatefulWidget {
  SearchFood({Key key}) : super(key: key);

  @override
  _SearchFoodState createState() => _SearchFoodState();
}

class _SearchFoodState extends State<SearchFood> {
  String search;
  List<FoodModel> foods = [];
  final storeController = Get.put(StoreController());

  _getFood() {
    if (storeController.foods.isEmpty) {
      storeController.getAllFood();
    }
  }

  _searProduct() {
    search != ''
        ? setState(() {
            foods = storeController.foods
                .where((element) =>
                    TiengViet.parse(element.nameFood.toLowerCase())
                        .contains(TiengViet.parse(search.toLowerCase())))
                .toList();
          })
        : setState(() => foods = []);
  }

  @override
  void initState() {
    super.initState();
    _getFood();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Tra Cứu Món Ăn Vặt'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(child: Text('Tìm Món Ăn')),
                          Container(
                            height: 45,
                            padding: EdgeInsets.only(left: 5, bottom: 10),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              onChanged: (value) {
                                search = value;
                                _searProduct();
                              },
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text('Danh Sách Món Ăn')),
                          ListFood(foods: foods, isImport: true)
                        ]))),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ImportFood(food: null)),
                  );
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.cyan[500])));
  }
}
