import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tiengviet/tiengviet.dart';

import 'package:banhistore/models/product.model.dart';
import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/views/share/products.view.dart';
import 'package:banhistore/views/store/import-grocery/import.view.dart';

class SearchProduct extends StatefulWidget {
  SearchProduct({Key key}) : super(key: key);

  @override
  _SearchProductState createState() => _SearchProductState();
}

class _SearchProductState extends State<SearchProduct> {
  String search;
  List<ProductModel> products = [];
  final storeController = Get.put(StoreController());

  _searProduct() {
    search != ''
        ? setState(() {
            products = storeController.products.value
                .where((element) =>
                    TiengViet.parse(element.productName.toLowerCase())
                        .contains(TiengViet.parse(search.toLowerCase())))
                .toList();
          })
        : setState(() => {products = []});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Tra Cứu Sản Phẩm'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(child: Text('Tìm Sản Phẩm')),
                          Container(
                            height: 45,
                            padding: EdgeInsets.only(left: 5, bottom: 10),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              onChanged: (value) {
                                search = value;
                                _searProduct();
                              },
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text('Danh Sách Sản Phẩm')),
                          Products(products: products, isImport: true)
                        ]))),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ImportProduct(product: null)),
                  );
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.cyan[500])));
  }
}
