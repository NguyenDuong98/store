import 'dart:io';
import 'package:path/path.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/utility/general.utility.dart';
import 'package:banhistore/models/product.model.dart';

class ImportProduct extends StatefulWidget {
  final ProductModel product;
  ImportProduct({Key key, this.product}) : super(key: key);

  @override
  _ImportProductState createState() => _ImportProductState();
}

class _ImportProductState extends State<ImportProduct> {
  bool isUpdate = true;
  String nameProduct = '';
  String cost = '';
  String price = '';
  String supplier = '';
  String amount = '';
  File imageProduct;
  String errorName = '';
  String errorCost = '';
  String errorPrice = '';
  String errorSupplier = '';
  String errorAmount = '';
  String errorImage = '';

  GeneralUtility generalUtility = GeneralUtility();
  final storeController = Get.put(StoreController());

  var txtNameTextField = TextEditingController();
  var txtCostTextField = TextEditingController();
  var txtPriceTextField = TextEditingController();
  var txtSupplierTextField = TextEditingController();
  var txtAmountTextField = TextEditingController();

  _getData() {
    if (widget.product != null) {
      setState(() {
        isUpdate = false;
        nameProduct = widget.product.productName;
        cost = widget.product.cost;
        price = widget.product.price;
        supplier = widget.product.supplier;
        txtNameTextField.text = widget.product.productName;
        txtCostTextField.text = widget.product.cost;
        txtPriceTextField.text = widget.product.price;
        txtSupplierTextField.text = widget.product.supplier;
      });
    }
  }

  _getImageByCamera(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    ImagePicker.pickImage(source: ImageSource.camera)
        .then((value) => setState(() {
              imageProduct = value;
            }));
    Navigator.pop(context);
  }

  _getImageFromAlbum(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    ImagePicker.pickImage(source: ImageSource.gallery)
        .then((value) => setState(() {
              imageProduct = value;
            }));
    Navigator.pop(context);
  }

  _validate() {
    if (nameProduct == '') {
      setState(() {
        errorName = 'Vui Lòng Nhập Tên Sản Phẩm';
      });
    } else {
      setState(() {
        errorName = '';
      });
    }

    if (cost == '') {
      setState(() {
        errorCost = 'Vui Lòng Nhập Giá Nhập Hàng';
      });
    } else {
      setState(() {
        errorCost = '';
      });
    }

    if (price == '') {
      setState(() {
        errorPrice = 'Vui Lòng Nhập Giá Bán';
      });
    } else {
      setState(() {
        errorPrice = '';
      });
    }

    if (supplier == '') {
      setState(() {
        errorSupplier = 'Vui Lòng Nhập Nhà Cung Cấp';
      });
    } else {
      setState(() {
        errorSupplier = '';
      });
    }

    if (amount == '') {
      setState(() {
        errorAmount = 'Vui Lòng Nhập Số Lượng';
      });
    } else {
      setState(() {
        errorAmount = '';
      });
    }

    if (imageProduct == null && widget.product == null) {
      setState(() {
        errorImage = 'Vui Lòng Chụp Hình Sản Phẩm';
      });
    } else {
      setState(() {
        errorImage = '';
      });
    }
  }

  _addProduct(BuildContext context) async {
    _validate();

    if (errorName == '' &&
        errorSupplier == '' &&
        errorAmount == '' &&
        errorImage == '' &&
        errorCost == '' &&
        errorPrice == '') {
      Map data = {
        'productName': nameProduct,
        'cost': cost,
        'price': price,
        'supplier': supplier,
        'amount': widget.product == null
            ? amount
            : (int.parse(widget.product.amount) + int.parse(amount)).toString(),
        'imageProduct': imageProduct == null
            ? null
            : ParseWebFile(await imageProduct.readAsBytes(),
                name: basename(imageProduct.path))
      };

      final response = widget.product == null
          ? await storeController.importProduct(data)
          : await storeController.updateProduct(widget.product.objectId, data);
      if (response != null) {
        Navigator.of(context).pop();
        generalUtility.showNotification('Nhập Hàng Thành Công');
      } else {
        generalUtility.showNotification('Đã Xãy Ra Lỗi Khi Nhập Hàng');
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Nhập Hàng'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [buildImportProduct(context)])))));
  }

  Widget buildImportProduct(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        child: Column(children: [
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Tên Sản Phẩm')),
            Container(
                child: Text(errorName,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          enabled: isUpdate,
          controller: txtNameTextField,
          onChanged: (value) {
            nameProduct = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Giá Gốc')),
            Container(
                child: Text(errorCost,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          controller: txtCostTextField,
          onChanged: (value) {
            cost = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Giá Bán')),
            Container(
                child: Text(errorPrice,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          controller: txtPriceTextField,
          onChanged: (value) {
            price = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Nhà Cung Cấp')),
            Container(
                child: Text(errorSupplier,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          enabled: isUpdate,
          controller: txtSupplierTextField,
          onChanged: (value) {
            supplier = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Số Lượng')),
            Container(
                child: Text(errorAmount,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
            controller: txtAmountTextField,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              amount = value;
            },
            decoration: new InputDecoration(
                border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Colors.greenAccent, width: 1))),
            style: TextStyle(height: 1, color: Colors.black, fontSize: 18)),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Ảnh Sản Phẩm')),
            Container(
                child: Text(errorImage,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      InkWell(
          onTap: () {
            showModalBottomSheet<void>(
              context: context,
              isScrollControlled: true,
              builder: (context) {
                return buildSelectImage(context);
              },
            );
          },
          child: Container(
              margin: EdgeInsets.only(top: 5),
              child: imageProduct == null && widget.product == null
                  ? Container(
                      height: width - 20,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              offset: const Offset(1, 1),
                              blurRadius: 5),
                        ],
                      ),
                      child: Icon(Icons.add_a_photo))
                  : Container(
                      height: width - 20,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                offset: const Offset(1, 1),
                                blurRadius: 5),
                          ],
                          image: DecorationImage(
                              image: widget.product == null
                                  ? FileImage(imageProduct)
                                  : NetworkImage(widget.product.imageProduct),
                              fit: BoxFit.cover))))),
      Container(
          width: width,
          height: 45,
          margin: EdgeInsets.only(top: 10, bottom: 10),
          child: RaisedButton(
            color: Colors.blue,
            onPressed: () {
              _addProduct(context);
            },
            child: Text('Thêm Sản Phẩm', style: TextStyle(color: Colors.white)),
          ))
    ]));
  }

  Widget buildSelectImage(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.12,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            child: InkWell(
              onTap: () {
                _getImageByCamera(context);
              },
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Image.asset('assets/images/camera.png')),
                  Container(
                    child: Text('Camera', style: TextStyle(fontSize: 18)),
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: InkWell(
              onTap: () {
                _getImageFromAlbum(context);
              },
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Image.asset('assets/images/gallery.png')),
                  Container(
                    child: Text('Thư viện', style: TextStyle(fontSize: 18)),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
