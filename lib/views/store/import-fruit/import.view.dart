import 'dart:io';
import 'package:path/path.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/utility/general.utility.dart';
import 'package:banhistore/models/fruit.model.dart';

class ImportFruit extends StatefulWidget {
  final FruitModel fruit;
  ImportFruit({Key key, this.fruit}) : super(key: key);

  @override
  _ImportFruitState createState() => _ImportFruitState();
}

class _ImportFruitState extends State<ImportFruit> {
  bool isUpdate = true;
  String nameFruit = '';
  String origin = '';
  String cost = '';
  String price = '';
  String status = 'Đang Bán';
  File imageFruit;
  String errornameFruit = '';
  String errorOrigin = '';
  String errorCost = '';
  String errorPrice = '';
  String errorImage = '';
  List<String> listStatus = ['Đang Bán', 'Tạm Ngưng'];

  GeneralUtility generalUtility = GeneralUtility();
  final storeController = Get.put(StoreController());

  var txtNameTextField = TextEditingController();
  var txtCostTextField = TextEditingController();
  var txtPriceTextField = TextEditingController();
  var txtOriginTextField = TextEditingController();

  _getData() {
    if (widget.fruit != null) {
      setState(() {
        isUpdate = false;
        nameFruit = widget.fruit.fruitName;
        cost = widget.fruit.cost;
        price = widget.fruit.price;
        origin = widget.fruit.origin;
        status = widget.fruit.status == true ? listStatus[0] : listStatus[1];
        txtNameTextField.text = widget.fruit.fruitName;
        txtCostTextField.text = widget.fruit.cost;
        txtPriceTextField.text = widget.fruit.price;
        txtOriginTextField.text = widget.fruit.origin;
      });
    }
  }

  _getImageByCamera(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    ImagePicker.pickImage(source: ImageSource.camera)
        .then((value) => setState(() {
              imageFruit = value;
            }));
    Navigator.pop(context);
  }

  _getImageFromAlbum(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    ImagePicker.pickImage(source: ImageSource.gallery)
        .then((value) => setState(() {
              imageFruit = value;
            }));
    Navigator.pop(context);
  }

  _validate() {
    if (nameFruit == '') {
      setState(() {
        errornameFruit = 'Vui Lòng Nhập Tên Trái Cây';
      });
    } else {
      setState(() {
        errornameFruit = '';
      });
    }

    if (origin == '') {
      setState(() {
        errorOrigin = 'Vui Lòng Nhập Nơi Xuất Xứ';
      });
    } else {
      setState(() {
        errorOrigin = '';
      });
    }

    if (cost == '') {
      setState(() {
        errorCost = 'Vui Lòng Nhập Giá Nhập Hàng';
      });
    } else {
      setState(() {
        errorCost = '';
      });
    }

    if (price == '') {
      setState(() {
        errorPrice = 'Vui Lòng Nhập Giá Bán';
      });
    } else {
      setState(() {
        errorPrice = '';
      });
    }

    if (imageFruit == null && widget.fruit == null) {
      setState(() {
        errorImage = 'Vui Lòng Chụp Hình Sản Phẩm';
      });
    } else {
      setState(() {
        errorImage = '';
      });
    }
  }

  _addProduct(BuildContext context) async {
    _validate();

    if (errornameFruit == '' &&
        errorOrigin == '' &&
        errorImage == '' &&
        errorCost == '' &&
        errorPrice == '') {
      Map data = {
        'fruitName': nameFruit,
        'origin': origin,
        'cost': cost,
        'price': price,
        'status': status == listStatus[0] ? true : false,
        'imageFruit': imageFruit == null
            ? null
            : ParseWebFile(await imageFruit.readAsBytes(),
                name: basename(imageFruit.path))
      };

      final response = widget.fruit == null
          ? await storeController.importFruit(data)
          : await storeController.updateFruit(widget.fruit.objectId, data);
      if (response != null) {
        Navigator.of(context).pop();
        generalUtility.showNotification('Nhập Trái Cây Thành Công');
      } else {
        generalUtility.showNotification('Đã Xãy Ra Lỗi Khi Nhập Trái Cây');
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Nhập Hàng'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [buildImportProduct(context)])))));
  }

  Widget buildImportProduct(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        child: Column(children: [
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Tên Trái Cây')),
            Container(
                child: Text(errornameFruit,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          enabled: isUpdate,
          controller: txtNameTextField,
          onChanged: (value) {
            nameFruit = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Xuất Xứ')),
            Container(
                child: Text(errorOrigin,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          enabled: isUpdate,
          controller: txtOriginTextField,
          onChanged: (value) {
            origin = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Trạng Thái')),
          ])),
      Container(
          width: width,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(5)),
          padding: EdgeInsets.only(left: 5),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              hint: Text(status,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w700)),
              items: listStatus.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  status = newValue;
                });
              },
            ),
          )),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Giá Gốc')),
            Container(
                child: Text(errorCost,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          controller: txtCostTextField,
          onChanged: (value) {
            cost = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Giá Bán')),
            Container(
                child: Text(errorPrice,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      Container(
        height: 50,
        child: TextField(
          controller: txtPriceTextField,
          onChanged: (value) {
            price = value;
          },
          decoration: new InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.greenAccent, width: 1))),
          style: TextStyle(height: 1, color: Colors.black, fontSize: 18),
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(children: [
            Expanded(child: const Text('Ảnh Trái Cây')),
            Container(
                child: Text(errorImage,
                    style: TextStyle(
                        color: Colors.red, fontStyle: FontStyle.italic)))
          ])),
      InkWell(
          onTap: () {
            showModalBottomSheet<void>(
              context: context,
              isScrollControlled: true,
              builder: (context) {
                return buildSelectImage(context);
              },
            );
          },
          child: Container(
              margin: EdgeInsets.only(top: 5),
              child: imageFruit == null && widget.fruit == null
                  ? Container(
                      height: width - 20,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              offset: const Offset(1, 1),
                              blurRadius: 5),
                        ],
                      ),
                      child: Icon(Icons.add_a_photo))
                  : Container(
                      height: width - 20,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                offset: const Offset(1, 1),
                                blurRadius: 5),
                          ],
                          image: DecorationImage(
                              image: widget.fruit == null
                                  ? FileImage(imageFruit)
                                  : NetworkImage(widget.fruit.imageFruit),
                              fit: BoxFit.cover))))),
      Container(
          width: width,
          height: 45,
          margin: EdgeInsets.only(top: 10, bottom: 10),
          child: RaisedButton(
            color: Colors.blue,
            onPressed: () {
              _addProduct(context);
            },
            child: Text('Thêm Sản Phẩm', style: TextStyle(color: Colors.white)),
          ))
    ]));
  }

  Widget buildSelectImage(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: height * 0.12,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            child: InkWell(
              onTap: () {
                _getImageByCamera(context);
              },
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Image.asset('assets/images/camera.png')),
                  Container(
                    child: Text('Camera', style: TextStyle(fontSize: 18)),
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: InkWell(
              onTap: () {
                _getImageFromAlbum(context);
              },
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Image.asset('assets/images/gallery.png')),
                  Container(
                    child: Text('Thư viện', style: TextStyle(fontSize: 18)),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
