import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:tiengviet/tiengviet.dart';
import 'package:get/get.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/views/store/import-fruit/import.view.dart';
import 'package:banhistore/models/fruit.model.dart';
import 'package:banhistore/views/share/fruits.view.dart';

class SearchFruit extends StatefulWidget {
  SearchFruit({Key key}) : super(key: key);

  @override
  _SearchFruitState createState() => _SearchFruitState();
}

class _SearchFruitState extends State<SearchFruit> {
  String search;
  List<FruitModel> fruits = [];
  final storeController = Get.put(StoreController());

  _getFruit() {
    if (storeController.fruits.isEmpty) {
      storeController.getAllFruit();
    }
  }

  _searProduct() {
    search != ''
        ? setState(() {
            fruits = storeController.fruits
                .where((element) =>
                    TiengViet.parse(element.fruitName.toLowerCase())
                        .contains(TiengViet.parse(search.toLowerCase())))
                .toList();
          })
        : setState(() => {fruits = []});
  }

  @override
  void initState() {
    super.initState();
    _getFruit();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text('Tra Cứu Trái Cây'),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(child: Text('Tìm Trái Cây')),
                          Container(
                            height: 45,
                            padding: EdgeInsets.only(left: 5, bottom: 10),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextField(
                              onChanged: (value) {
                                search = value;
                                _searProduct();
                              },
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                              style: TextStyle(
                                  height: 1, color: Colors.black, fontSize: 18),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text('Danh Sách Trái Cây')),
                          ListFruit(fruits: fruits, isImport: true)
                        ]))),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ImportFruit(fruit: null)),
                  );
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.cyan[500])));
  }
}
