import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tiengviet/tiengviet.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/views/share/fruits.view.dart';
import 'package:banhistore/models/fruit.model.dart';

class Fruits extends StatefulWidget {
  final Function addToCart;
  Fruits({Key key, this.addToCart}) : super(key: key);

  @override
  _FruitsState createState() => _FruitsState();
}

class _FruitsState extends State<Fruits> {
  String search;
  List<FruitModel> fruits;
  final storeController = Get.put(StoreController());

  _getAllFruit() {
    if (storeController.fruits.isEmpty) {
      storeController.getAllFruit().then((value) => setState(() {
            fruits = value;
          }));
    } else {
      setState(() {
        fruits = storeController.fruits;
      });
    }
  }

  _search() {
    if (search == '') {
      setState(() {
        fruits = storeController.fruits;
      });
    } else {
      setState(() {
        fruits = storeController.fruits
            .where((element) => TiengViet.parse(element.fruitName.toLowerCase())
                .contains(TiengViet.parse(search.toLowerCase())))
            .toList();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getAllFruit();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxScrolled) {
                  return <Widget>[createSilverAppBar1(), createSilverAppBar2()];
                },
                body: Container(
                  child: fruits == null
                      ? Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 1.5))
                      : ListFruit(
                          fruits: fruits,
                          isImport: false,
                          addToCart: widget.addToCart),
                ))));
  }

  SliverAppBar createSilverAppBar1() {
    return SliverAppBar(
      backgroundColor: Colors.redAccent.withOpacity(0),
      expandedHeight: 150,
      floating: false,
      elevation: 0,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: Container(
            color: Colors.white,
            child: Image.asset(
              'assets/images/banner_fruit.png',
              fit: BoxFit.cover,
            ),
          ),
        );
      }),
    );
  }

  SliverAppBar createSilverAppBar2() {
    return SliverAppBar(
        backgroundColor: Colors.white.withOpacity(0.5),
        pinned: true,
        title: Container(
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  offset: const Offset(1, 1),
                  blurRadius: 5),
            ],
          ),
          child: CupertinoTextField(
            onChanged: (value) {
              search = value;
              _search();
            },
            keyboardType: TextInputType.text,
            placeholder: 'Search',
            placeholderStyle: TextStyle(
              color: Color(0xffC4C6CC),
              fontSize: 14.0,
              fontFamily: 'Brutal',
            ),
            prefix: Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Icon(
                Icons.search,
                size: 18,
                color: Colors.black,
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
            ),
          ),
        ));
  }
}
