import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tiengviet/tiengviet.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/views/share/products.view.dart';
import 'package:banhistore/models/product.model.dart';

class Grocery extends StatefulWidget {
  final Function addToCart;
  Grocery({Key key, this.addToCart}) : super(key: key);

  @override
  _GroceryState createState() => _GroceryState();
}

class _GroceryState extends State<Grocery> {
  String search;
  List<ProductModel> products;
  final storeController = Get.put(StoreController());

  _getAllProduct() {
    if (storeController.products.isEmpty) {
      storeController.getAllProduct().then((value) => setState(() {
            products = value;
          }));
    } else {
      setState(() {
        products = storeController.products;
      });
    }
  }

  _search() {
    if (search == '') {
      products = storeController.products;
    } else {
      setState(() {
        products = storeController.products
            .where((element) =>
                TiengViet.parse(element.productName.toLowerCase())
                    .contains(TiengViet.parse(search.toLowerCase())))
            .toList();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getAllProduct();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxScrolled) {
                  return <Widget>[createSilverAppBar1(), createSilverAppBar2()];
                },
                body: Container(
                  child: products == null
                      ? Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 1.5))
                      : Products(
                          addToCart: widget.addToCart,
                          products: products,
                          isImport: false),
                ))));
  }

  SliverAppBar createSilverAppBar1() {
    return SliverAppBar(
      backgroundColor: Colors.redAccent.withOpacity(0),
      expandedHeight: 150,
      floating: false,
      elevation: 0,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: Container(
            color: Colors.white,
            child: Image.asset(
              'assets/images/banner.png',
              fit: BoxFit.cover,
            ),
          ),
        );
      }),
    );
  }

  SliverAppBar createSilverAppBar2() {
    return SliverAppBar(
        backgroundColor: Colors.white.withOpacity(0.5),
        pinned: true,
        title: Container(
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  offset: const Offset(1, 1),
                  blurRadius: 5),
            ],
          ),
          child: CupertinoTextField(
            onChanged: (value) {
              search = value;
              _search();
            },
            keyboardType: TextInputType.text,
            placeholder: 'Search',
            placeholderStyle: TextStyle(
              color: Color(0xffC4C6CC),
              fontSize: 14.0,
              fontFamily: 'Brutal',
            ),
            prefix: Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Icon(
                Icons.search,
                size: 18,
                color: Colors.black,
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
            ),
          ),
        ));
  }
}
