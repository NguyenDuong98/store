import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/controllers/cart.controller.dart';
import 'package:banhistore/views/store/import-food/import.view.dart';

class ListFood extends StatefulWidget {
  final List<FoodModel> foods;
  final bool isImport;
  final Function addToCart;
  ListFood({Key key, this.foods, this.isImport, this.addToCart})
      : super(key: key);

  @override
  _ListFoodState createState() => _ListFoodState();
}

class _ListFoodState extends State<ListFood> {
  final cartController = Get.put(CartController());
  final oCcy = NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            padding: EdgeInsets.zero,
            children: buildListFoods()));
  }

  List<Widget> buildListFoods() {
    return widget.foods.map((food) => buildFood(food)).toList();
  }

  Widget buildFood(FoodModel food) {
    double width = MediaQuery.of(context).size.width;
    return InkWell(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (widget.isImport) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ImportFood(food: food)));
          } else {
            cartController.addFoodToCart(food);
            widget.addToCart();
          }
        },
        child: Container(
            height: 100,
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(color: Colors.white),
            child: Row(children: [
              Expanded(
                  flex: 3,
                  child: Stack(children: [
                    Positioned(
                        child: Container(
                            height: 100,
                            width: width * 0.3,
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0, 0),
                                  blurRadius: 6,
                                ),
                              ],
                            ),
                            child: CachedNetworkImage(
                              imageUrl: food.imageFood,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) => Container(
                                alignment: Alignment.center,
                                child:
                                    CircularProgressIndicator(strokeWidth: 1.5),
                              ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ))),
                    food.status
                        ? Container()
                        : Positioned(
                            child:
                                Container(color: Colors.grey.withOpacity(0.6)))
                  ])),
              Expanded(
                  flex: 7,
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              child: Text(food.nameFood,
                                  style: TextStyle(
                                      color: food.status
                                          ? Colors.cyan[600]
                                          : Colors.grey,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis)),
                          Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: 'Giá: ',
                                        style: TextStyle(fontSize: 18)),
                                    TextSpan(
                                        text:
                                            oCcy.format(int.parse(food.price)) +
                                                'đ',
                                        style: TextStyle(
                                            color: food.status
                                                ? Colors.red
                                                : Colors.grey,
                                            fontSize: 18)),
                                  ],
                                ),
                              )),
                        ],
                      )))
            ])));
  }
}
