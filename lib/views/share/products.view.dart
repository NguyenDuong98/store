import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:banhistore/models/product.model.dart';
import 'package:banhistore/controllers/cart.controller.dart';
import 'package:banhistore/views/store/import-grocery/import.view.dart';

class Products extends StatefulWidget {
  final bool isImport;
  final List<ProductModel> products;
  final Function addToCart;
  Products({Key key, this.products, this.isImport, this.addToCart})
      : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  final oCcy = NumberFormat("#,##0", "en_US");
  final cartController = Get.put(CartController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            padding: EdgeInsets.zero,
            children: buildListProduct()));
  }

  List<Widget> buildListProduct() {
    return widget.products.map((product) => buildProduct(product)).toList();
  }

  Widget buildProduct(ProductModel product) {
    return InkWell(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (widget.isImport) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ImportProduct(product: product)));
          } else {
            cartController.addProductToCart(product);
            widget.addToCart();
          }
        },
        child: Container(
            height: 100,
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(color: Colors.white),
            child: Row(children: [
              Expanded(
                  flex: 3,
                  child: Container(
                      margin: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              offset: Offset(0, 0),
                              blurRadius: 6,
                            ),
                          ]),
                      child: CachedNetworkImage(
                        imageUrl: product.imageProduct,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                        placeholder: (context, url) => Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 1.5),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ))),
              Expanded(
                  flex: 7,
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              child: Text(product.productName,
                                  style: TextStyle(
                                      color: Colors.cyan[600],
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis)),
                          Container(
                              margin: EdgeInsets.only(top: 12, bottom: 12),
                              child: Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(text: 'Nhãn Hiệu: '),
                                    TextSpan(text: product.supplier),
                                  ],
                                ),
                                overflow: TextOverflow.ellipsis,
                              )),
                          Container(
                              child: Row(
                            children: [
                              Expanded(
                                  child: Text.rich(
                                TextSpan(
                                  children: [
                                    const TextSpan(text: 'Giá: '),
                                    TextSpan(
                                        text: oCcy.format(
                                                int.parse(product.price)) +
                                            'đ',
                                        style: TextStyle(color: Colors.red)),
                                  ],
                                ),
                              )),
                              Expanded(
                                  child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(text: 'Số Lượng Tồn: '),
                                    TextSpan(
                                        text: product.amount,
                                        style: TextStyle(color: Colors.red)),
                                  ],
                                ),
                              ))
                            ],
                          )),
                        ],
                      )))
            ])));
  }
}
