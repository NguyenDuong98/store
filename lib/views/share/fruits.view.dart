import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:banhistore/models/fruit.model.dart';
import 'package:banhistore/controllers/cart.controller.dart';
import 'package:banhistore/views/store/import-fruit/import.view.dart';

class ListFruit extends StatefulWidget {
  final List<FruitModel> fruits;
  final bool isImport;
  final Function addToCart;
  ListFruit({Key key, this.fruits, this.isImport, this.addToCart})
      : super(key: key);

  @override
  _ListFruitState createState() => _ListFruitState();
}

class _ListFruitState extends State<ListFruit> {
  final oCcy = NumberFormat("#,##0", "en_US");
  final cartController = Get.put(CartController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            padding: EdgeInsets.zero,
            children: buildListFruits()));
  }

  List<Widget> buildListFruits() {
    return widget.fruits.map((fruit) => buildFruit(fruit)).toList();
  }

  Widget buildFruit(FruitModel fruit) {
    double width = MediaQuery.of(context).size.width;
    return InkWell(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (widget.isImport) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ImportFruit(fruit: fruit)));
          } else {
            cartController.addFruitToCart(fruit);
            widget.addToCart();
          }
        },
        child: Container(
            height: 100,
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(color: Colors.white),
            child: Row(children: [
              Expanded(
                  flex: 3,
                  child: Stack(children: [
                    Positioned(
                        child: Container(
                            height: 100,
                            width: width * 0.3,
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0, 0),
                                  blurRadius: 6,
                                ),
                              ],
                            ),
                            child: CachedNetworkImage(
                              imageUrl: fruit.imageFruit,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) => Container(
                                alignment: Alignment.center,
                                child:
                                    CircularProgressIndicator(strokeWidth: 1.5),
                              ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ))),
                    fruit.status
                        ? Container()
                        : Positioned(
                            child:
                                Container(color: Colors.grey.withOpacity(0.6)))
                  ])),
              Expanded(
                  flex: 7,
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              child: Text(fruit.fruitName,
                                  style: TextStyle(
                                      color: fruit.status
                                          ? Colors.cyan[600]
                                          : Colors.grey,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis)),
                          Container(
                              margin: EdgeInsets.only(top: 12, bottom: 12),
                              child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(text: 'Xuất Sứ: '),
                                    TextSpan(text: fruit.origin),
                                  ],
                                ),
                                overflow: TextOverflow.ellipsis,
                              )),
                          Container(
                              child: Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(text: 'Giá: '),
                                TextSpan(
                                    text: oCcy.format(int.parse(fruit.price)) +
                                        'đ',
                                    style: TextStyle(
                                        color: fruit.status
                                            ? Colors.red
                                            : Colors.grey)),
                              ],
                            ),
                          )),
                        ],
                      )))
            ])));
  }
}
