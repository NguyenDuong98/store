import 'package:banhistore/models/customer.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'package:banhistore/controllers/cart.controller.dart';

class Pay extends StatefulWidget {
  final int totalMoney;
  final int refundMoney;
  Pay({Key key, this.totalMoney, this.refundMoney}) : super(key: key);

  @override
  _PayState createState() => _PayState();
}

class _PayState extends State<Pay> {
  bool isNewCustomer;
  String searchCustomer = '';
  String customerName = '';
  String cashback = '0';
  String useRefund = '0';
  String payment = '0';
  String errorUseRefund = '';
  CustomerModel customer;

  final oCcy = NumberFormat("#,##0", "en_US");
  final cartController = Get.put(CartController());

  _searchCustomer() async {
    cartController
        .searchCustomer(searchCustomer)
        .then((value) => setState(() => {
              if (value.customer != null)
                {
                  customer = value,
                  cashback = value.refund,
                  isNewCustomer = false,
                  customerName = value.customer,
                }
              else if (searchCustomer != null)
                {customerName = searchCustomer, isNewCustomer = true}
              else
                {isNewCustomer = null}
            }));
  }

  _getPayment() {
    setState(() {
      payment = (widget.totalMoney - int.parse(useRefund)).toString();
    });
  }

  _pay() {}

  @override
  void initState() {
    super.initState();
    _getPayment();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(title: Text('Thanh Toán'), centerTitle: true),
            body: Container(
                padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                child: SingleChildScrollView(
                    child: Column(children: [
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Row(children: [
                        Expanded(child: const Text('Tìm Khách Hàng')),
                        isNewCustomer == null
                            ? Container()
                            : Container(
                                child: Text(
                                    isNewCustomer
                                        ? 'Khách Hàng mới'
                                        : 'Khách Hàng Thân Thuộc',
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontStyle: FontStyle.italic)))
                      ])),
                  Container(
                    height: 50,
                    child: Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        Positioned(
                            child: TextField(
                          onChanged: (value) {
                            searchCustomer = value;
                          },
                          decoration: new InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.greenAccent, width: 1))),
                          style: TextStyle(
                              height: 1, color: Colors.black, fontSize: 18),
                        )),
                        Positioned(
                            child: InkWell(
                                onTap: () {
                                  _searchCustomer();
                                },
                                child: Container(
                                    width: 50,
                                    height: double.infinity,
                                    child: Icon(Icons.search))))
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      alignment: Alignment.centerLeft,
                      child: Text('Tên Khách Hàng')),
                  Container(
                      height: 50,
                      width: width,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey)),
                      child:
                          Text(customerName, style: TextStyle(fontSize: 16))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      alignment: Alignment.centerLeft,
                      child: Text('Hoàn Tiền Tích Lũy')),
                  Container(
                      height: 50,
                      width: width,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey)),
                      child: Text(oCcy.format(int.parse(cashback)) + 'đ',
                          style: TextStyle(fontSize: 16))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Row(children: [
                        Expanded(
                            child: const Text('Thành Tiền Đơn Hàng Hiện Tại')),
                      ])),
                  Container(
                      height: 50,
                      width: width,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey)),
                      child: Text(oCcy.format(widget.totalMoney) + 'đ',
                          style: TextStyle(fontSize: 16))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      alignment: Alignment.centerLeft,
                      child: Text('Tiền Tích Lũy Đơn Hàng Hiện Tại')),
                  Container(
                      height: 50,
                      width: width,
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey)),
                      child: Text(oCcy.format(widget.refundMoney) + 'đ',
                          style: TextStyle(fontSize: 16))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Row(children: [
                        Expanded(child: const Text('Áp Dụng Hoàn Tiền')),
                        Container(
                            child: Text(errorUseRefund,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontStyle: FontStyle.italic)))
                      ])),
                  Container(
                    height: 50,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          useRefund = value;
                          if (value == '') {
                            payment = widget.totalMoney.toString();
                          } else {
                            if ((isNewCustomer == false &&
                                    int.parse(useRefund) >
                                        int.parse(customer.refund)) ||
                                (isNewCustomer && value == '')) {
                              errorUseRefund = 'Tiền Tích Lũy Không Đủ';
                              payment = widget.totalMoney.toString();
                            } else {
                              errorUseRefund = '';
                              _getPayment();
                            }
                          }
                        });
                      },
                      decoration: new InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.greenAccent, width: 1))),
                      style: TextStyle(
                          height: 1, color: Colors.black, fontSize: 18),
                    ),
                  ),
                  Container(
                      height: 50,
                      width: width,
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(left: 10, right: 10),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(children: [
                        Expanded(
                            child: Text('Thành Tiền: ',
                                style: TextStyle(fontSize: 20))),
                        Text(oCcy.format(int.parse(payment)) + 'đ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.bold))
                      ])),
                  InkWell(
                      onTap: () {
                        _pay();
                      },
                      child: Container(
                          width: width,
                          height: 50,
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 0, bottom: 20),
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(5)),
                          child: Text('Thanh Toán',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold))))
                ])))));
  }
}
