import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:get/get.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:banhistore/controllers/cart.controller.dart';
import 'package:banhistore/models/product-cart.model.dart';
import 'package:banhistore/models/fruit-cart.model.dart';
import 'package:banhistore/utility/cart.utility.dart';
import 'package:banhistore/models/refund.model.dart';
import 'package:banhistore/models/food-cart.model.dart';
import 'package:banhistore/views/pay/pay.view.dart';
import 'package:banhistore/controllers/store.controller.dart';

class Cart extends StatefulWidget {
  Cart({Key key}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  bool reRender;
  int totalMoney = 0;
  int refundMoney = 0;
  RefundModel refund;
  final oCcy = NumberFormat("#,##0", "en_US");
  final cartutility = CartUtility();
  final cartController = Get.put(CartController());
  final storeController = Get.put(StoreController());

  _getRefund() async {
    if (storeController.refund.value.grocery != null) {
      setState(() {
        refund = storeController.refund.value;
      });
    } else {
      await storeController.getRefund().then((value) => setState(() {
            refund = value[0];
          }));
    }
    setState(() {
      totalMoney = cartutility.getToTalMoney();
      refundMoney = cartutility.getRefundMoney(refund).toInt();
    });
  }

  @override
  void initState() {
    super.initState();
    _getRefund();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.only(top: 30, bottom: 30),
        child: AlertDialog(
          title:
              Container(alignment: Alignment.center, child: Text('Giỏ Hàng')),
          insetPadding: EdgeInsets.only(left: 10, right: 10),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          content: Builder(
            builder: (context) {
              return Container(
                  width: width,
                  margin: EdgeInsets.only(top: 20, bottom: 20),
                  child: cartutility.getToTalProduct() == 0
                      ? Container(
                          child: Text('Chưa Có Sản Phẩm Nào Trong Giỏ Hàng',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontStyle: FontStyle.italic)))
                      : SingleChildScrollView(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                              Container(
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  margin: EdgeInsets.only(bottom: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: const Offset(1, 1),
                                            blurRadius: 5),
                                      ]),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        cartController.productsInCart.isEmpty
                                            ? Container()
                                            : Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 10),
                                                child: Text('Tạp Hóa',
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        color:
                                                            Colors.orange[500],
                                                        fontWeight:
                                                            FontWeight.bold))),
                                        ListView(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics:
                                                const ClampingScrollPhysics(),
                                            padding: EdgeInsets.zero,
                                            children: cartController
                                                .productsInCart
                                                .asMap()
                                                .entries
                                                .map((productCart) =>
                                                    buildPoduct(
                                                        productCart.value,
                                                        productCart.key))
                                                .toList())
                                      ])),
                              Container(
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  margin: EdgeInsets.only(bottom: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: const Offset(1, 1),
                                            blurRadius: 5),
                                      ]),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        cartController.fruitsInCart.isEmpty
                                            ? Container()
                                            : Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 10),
                                                child: Text('Trái Cây',
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        color:
                                                            Colors.orange[500],
                                                        fontWeight:
                                                            FontWeight.bold))),
                                        ListView(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics:
                                                const ClampingScrollPhysics(),
                                            padding: EdgeInsets.zero,
                                            children: cartController
                                                .fruitsInCart
                                                .asMap()
                                                .entries
                                                .map((fruitCart) => buildFruit(
                                                    fruitCart.value,
                                                    fruitCart.key))
                                                .toList())
                                      ])),
                              Container(
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            offset: const Offset(1, 1),
                                            blurRadius: 5),
                                      ]),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        cartController.foodsInCart.isEmpty
                                            ? Container()
                                            : Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 10),
                                                child: Text('Ăn Vặt',
                                                    style: TextStyle(
                                                        fontSize: 22,
                                                        color:
                                                            Colors.orange[500],
                                                        fontWeight:
                                                            FontWeight.bold))),
                                        ListView(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics:
                                                const ClampingScrollPhysics(),
                                            padding: EdgeInsets.zero,
                                            children: cartController.foodsInCart
                                                .asMap()
                                                .entries
                                                .map((foodCart) => buildFood(
                                                    foodCart.value,
                                                    foodCart.key))
                                                .toList())
                                      ])),
                              Container(
                                  width: width,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 5, bottom: 5),
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.orange[500],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Expanded(
                                              child: Text('Thành Tiền: ',
                                                  style:
                                                      TextStyle(fontSize: 20))),
                                          Text(oCcy.format(totalMoney) + 'đ',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 22,
                                                  fontWeight: FontWeight.bold))
                                        ]),
                                        Row(children: [
                                          Expanded(
                                              child: Text('Hoàn Tiền: ',
                                                  style:
                                                      TextStyle(fontSize: 20))),
                                          storeController
                                                      .refund.value.grocery ==
                                                  null
                                              ? CircularProgressIndicator(
                                                  strokeWidth: 1.5)
                                              : Text(
                                                  oCcy.format(refundMoney) +
                                                      'đ',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.bold))
                                        ])
                                      ])),
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Pay(
                                                totalMoney: totalMoney,
                                                refundMoney: refundMoney)));
                                  },
                                  child: Container(
                                      width: width,
                                      height: 50,
                                      alignment: Alignment.center,
                                      margin:
                                          EdgeInsets.only(top: 0, bottom: 20),
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Text('Đặt Hàng',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold))))
                            ])));
            },
          ),
        ));
  }

  Widget buildPoduct(ProductCartModel productCart, int index) {
    return Container(
        height: 70,
        margin: EdgeInsets.only(bottom: 5),
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: index == cartController.productsInCart.length - 1
                        ? Colors.white
                        : Colors.grey[300]))),
        child: Row(children: [
          Expanded(
              flex: 2,
              child: Container(
                  child: CachedNetworkImage(
                imageUrl: productCart.product.imageProduct,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                placeholder: (context, url) => Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(strokeWidth: 1.5),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ))),
          Expanded(
              flex: 8,
              child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Row(
                          children: [
                            Expanded(
                                child: Text(productCart.product.productName,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.cyan[600],
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500))),
                            Container(
                                child: Container(
                                    child: Row(children: [
                              InkWell(
                                onTap: () {
                                  cartController
                                      .removeProductInCart(productCart.product);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('-')),
                              ),
                              Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(right: 5, left: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(3)),
                                  child: Text(productCart.amount.toString())),
                              InkWell(
                                onTap: () {
                                  cartController
                                      .addProductToCart(productCart.product);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('+')),
                              ),
                            ])))
                          ],
                        )),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(children: [
                              Expanded(
                                  child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: 'Giá: ',
                                        style: TextStyle(fontSize: 16)),
                                    TextSpan(
                                        text: oCcy.format(int.parse(
                                                productCart.product.price)) +
                                            'đ',
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 16)),
                                  ],
                                ),
                              )),
                              Container(
                                  child: Text(productCart.product.supplier))
                            ])),
                      ])))
        ]));
  }

  Widget buildFruit(FruitCartModel fruitCart, int index) {
    return Container(
        height: 70,
        margin: EdgeInsets.only(bottom: 5),
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: index == cartController.fruitsInCart.length - 1
                        ? Colors.white
                        : Colors.grey[300]))),
        child: Row(children: [
          Expanded(
              flex: 2,
              child: Container(
                  child: CachedNetworkImage(
                imageUrl: fruitCart.fruit.imageFruit,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                placeholder: (context, url) => Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(strokeWidth: 1.5),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ))),
          Expanded(
              flex: 8,
              child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Row(
                          children: [
                            Expanded(
                                child: Text(fruitCart.fruit.fruitName,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.cyan[600],
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500))),
                            Container(
                                child: Container(
                                    child: Row(children: [
                              InkWell(
                                onTap: () {
                                  cartController
                                      .removeFruitInCart(fruitCart.fruit);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('-')),
                              ),
                              Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(right: 5, left: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(3)),
                                  child: Text(fruitCart.amount.toString())),
                              InkWell(
                                onTap: () {
                                  cartController
                                      .addFruitToCart(fruitCart.fruit);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('+')),
                              ),
                            ])))
                          ],
                        )),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(children: [
                              Expanded(
                                  child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: 'Giá: ',
                                        style: TextStyle(fontSize: 16)),
                                    TextSpan(
                                        text: oCcy.format(int.parse(
                                                fruitCart.fruit.price)) +
                                            'đ',
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 16)),
                                  ],
                                ),
                              )),
                              Container(child: Text(fruitCart.fruit.origin))
                            ])),
                      ])))
        ]));
  }

  Widget buildFood(FoodCartModel foodCart, int index) {
    return Container(
        height: 70,
        margin: EdgeInsets.only(bottom: 5),
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: index == cartController.foodsInCart.length - 1
                        ? Colors.white
                        : Colors.grey[300]))),
        child: Row(children: [
          Expanded(
              flex: 2,
              child: Container(
                  child: CachedNetworkImage(
                imageUrl: foodCart.food.imageFood,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                placeholder: (context, url) => Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(strokeWidth: 1.5),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ))),
          Expanded(
              flex: 8,
              child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Row(
                          children: [
                            Expanded(
                                child: Text(foodCart.food.nameFood,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.cyan[600],
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500))),
                            Container(
                                child: Container(
                                    child: Row(children: [
                              InkWell(
                                onTap: () {
                                  cartController
                                      .removeFoodInCart(foodCart.food);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('-')),
                              ),
                              Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(right: 5, left: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(3)),
                                  child: Text(foodCart.amount.toString())),
                              InkWell(
                                onTap: () {
                                  cartController.addFoodToCart(foodCart.food);
                                  setState(() {
                                    reRender = true;
                                  });
                                },
                                child: Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Text('+')),
                              ),
                            ])))
                          ],
                        )),
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Row(children: [
                              Expanded(
                                  child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: 'Giá: ',
                                        style: TextStyle(fontSize: 16)),
                                    TextSpan(
                                        text: oCcy.format(int.parse(
                                                foodCart.food.price)) +
                                            'đ',
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 16)),
                                  ],
                                ),
                              ))
                            ])),
                      ])))
        ]));
  }
}
