import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:banhistore/controllers/store.controller.dart';
import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/views/share/foods.view.dart';

class Food extends StatefulWidget {
  final Function addToCart;
  Food({Key key, this.addToCart}) : super(key: key);

  @override
  _FoodState createState() => _FoodState();
}

class _FoodState extends State<Food> {
  List<FoodModel> foods;
  final storeController = Get.put(StoreController());

  _getAllFood() {
    if (storeController.foods.isEmpty) {
      storeController.getAllFood().then((value) => setState(() {
            foods = value;
          }));
    } else {
      setState(() {
        foods = storeController.foods;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getAllFood();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxScrolled) {
                  return <Widget>[createSilverAppBar1(), createSilverAppBar2()];
                },
                body: Container(
                    child: foods == null
                        ? Container()
                        : ListFood(
                            foods: foods,
                            isImport: false,
                            addToCart: widget.addToCart)))));
  }

  SliverAppBar createSilverAppBar1() {
    return SliverAppBar(
      backgroundColor: Colors.redAccent.withOpacity(0),
      expandedHeight: 150,
      floating: false,
      elevation: 0,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: Container(
            color: Colors.white,
            child: Image.asset(
              'assets/images/banner_food.png',
              fit: BoxFit.cover,
            ),
          ),
        );
      }),
    );
  }

  SliverAppBar createSilverAppBar2() {
    return SliverAppBar(
        backgroundColor: Colors.white.withOpacity(0.5),
        pinned: true,
        title: Container(
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  offset: const Offset(1, 1),
                  blurRadius: 5),
            ],
          ),
          child: CupertinoTextField(
            keyboardType: TextInputType.text,
            placeholder: 'Search',
            placeholderStyle: TextStyle(
              color: Color(0xffC4C6CC),
              fontSize: 14.0,
              fontFamily: 'Brutal',
            ),
            prefix: Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
              child: Icon(
                Icons.search,
                size: 18,
                color: Colors.black,
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
            ),
          ),
        ));
  }
}
