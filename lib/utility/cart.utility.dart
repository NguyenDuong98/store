import 'package:banhistore/models/refund.model.dart';
import 'package:get/get.dart';
import 'package:banhistore/controllers/cart.controller.dart';

class CartUtility {
  final cartController = Get.put(CartController());

  int getToTalProduct() {
    final totalProduct =
        cartController.productsInCart.fold(0, (sum, item) => sum + item.amount);
    final totalFruit =
        cartController.fruitsInCart.fold(0, (sum, item) => sum + item.amount);
    final totalFood =
        cartController.foodsInCart.fold(0, (sum, item) => sum + item.amount);
    final result = totalProduct + totalFruit + totalFood;
    return result;
  }

  int getToTalMoney() {
    final totalProduct = cartController.productsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.product.price)));
    final totalFruit = cartController.fruitsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.fruit.price)));
    final totalFood = cartController.foodsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.food.price)));
    final result = totalProduct + totalFruit + totalFood;
    return result;
  }

  double getRefundMoney(RefundModel refund) {
    final totalProduct = cartController.productsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.product.price)));
    final totalFruit = cartController.fruitsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.fruit.price)));
    final totalFood = cartController.foodsInCart.fold(
        0, (sum, item) => sum + (item.amount * int.parse(item.food.price)));

    final refundMoney = totalProduct * double.parse(refund.grocery) +
        totalFruit * double.parse(refund.fruit) +
        totalFood * double.parse(refund.food);
    return refundMoney;
  }
}
