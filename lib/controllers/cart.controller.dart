import 'package:banhistore/models/customer.model.dart';
import 'package:get/get.dart';

import 'package:banhistore/models/fruit.model.dart';
import 'package:banhistore/models/product.model.dart';
import 'package:banhistore/models/product-cart.model.dart';
import 'package:banhistore/models/fruit-cart.model.dart';
import 'package:banhistore/models/food-cart.model.dart';
import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/services/cart.service.dart';

class CartController extends GetxController {
  var productsInCart = List<ProductCartModel>().obs;
  var fruitsInCart = List<FruitCartModel>().obs;
  var foodsInCart = List<FoodCartModel>().obs;
  CartServices cartServices = CartServices();

  //-------------- Products --------------//
  addProductToCart(ProductModel product) {
    final index = productsInCart
        .indexWhere((element) => element.product.objectId == product.objectId);
    if (index == -1) {
      final productCart = ProductCartModel(amount: 1, product: product);
      productsInCart.add(productCart);
    } else {
      productsInCart[index].amount += 1;
    }
  }

  removeProductInCart(ProductModel product) {
    final index = productsInCart
        .indexWhere((element) => element.product.objectId == product.objectId);
    if (productsInCart[index].amount > 1) {
      productsInCart[index].amount -= 1;
    } else {
      productsInCart.removeAt(index);
    }
  }

  //--------------- Fruits -----------------//
  addFruitToCart(FruitModel fruit) {
    final index = fruitsInCart
        .indexWhere((element) => element.fruit.objectId == fruit.objectId);
    if (index == -1) {
      final fruitCart = FruitCartModel(amount: 1, fruit: fruit);
      fruitsInCart.add(fruitCart);
    } else {
      fruitsInCart[index].amount += 1;
    }
  }

  removeFruitInCart(FruitModel fruit) {
    final index = fruitsInCart
        .indexWhere((element) => element.fruit.objectId == fruit.objectId);
    if (fruitsInCart[index].amount > 1) {
      fruitsInCart[index].amount -= 1;
    } else {
      fruitsInCart.removeAt(index);
    }
  }

  //--------------- Foods -----------------//
  addFoodToCart(FoodModel food) {
    final index = foodsInCart
        .indexWhere((element) => element.food.objectId == food.objectId);
    if (index == -1) {
      final foodCart = FoodCartModel(amount: 1, food: food);
      foodsInCart.add(foodCart);
    } else {
      foodsInCart[index].amount += 1;
    }
  }

  removeFoodInCart(FoodModel food) {
    final index = foodsInCart
        .indexWhere((element) => element.food.objectId == food.objectId);
    if (foodsInCart[index].amount > 1) {
      foodsInCart[index].amount -= 1;
    } else {
      foodsInCart.removeAt(index);
    }
  }

  //--------------- Payment -----------------//
  Future<CustomerModel> searchCustomer(String keySearch) async {
    return await cartServices.searchCustomer(keySearch);
  }
}
