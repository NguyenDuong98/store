import 'package:get/get.dart';

import 'package:banhistore/services/store.service.dart';
import 'package:banhistore/models/fruit.model.dart';
import 'package:banhistore/models/product.model.dart';
import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/models/refund.model.dart';

class StoreController extends GetxController {
  var products = <ProductModel>[].obs;
  var fruits = <FruitModel>[].obs;
  var foods = <FoodModel>[].obs;
  var refund = RefundModel().obs;
  StoreServices storeServices = StoreServices();

  Future<ProductModel> importProduct(Map product) async {
    final result = await storeServices.importProduct(product);
    products.add(result);
    return result;
  }

  Future<List<ProductModel>> getAllProduct() async {
    final result = await storeServices.getAllProduct();
    products.assignAll(result);
    return result;
  }

  Future<List<ProductModel>> searchProduct(String keySearch) async {
    return await storeServices.searchProduct(keySearch);
  }

  Future<dynamic> updateProduct(String objectId, Map data) async {
    final result = await storeServices.updateProduct(objectId, data);
    final indexProductUpdate =
        products.indexWhere((element) => element.objectId == objectId);
    products[indexProductUpdate].cost = data['cost'];
    products[indexProductUpdate].price = data['price'];
    products[indexProductUpdate].amount = data['amount'];
    if (data['imageProduct'] != null) {
      products[indexProductUpdate].imageProduct = result['imageProduct']['url'];
    }
    return result;
  }

  Future<FruitModel> importFruit(Map fruit) async {
    final result = await storeServices.importFruit(fruit);
    fruits.add(result);
    return result;
  }

  Future<List<FruitModel>> searchFruit(String keySearch) async {
    return await storeServices.searchFruit(keySearch);
  }

  Future<dynamic> updateFruit(String objectId, Map data) async {
    final result = await storeServices.updateFruit(objectId, data);
    final indexFruitUpdate =
        fruits.indexWhere((element) => element.objectId == objectId);
    fruits[indexFruitUpdate].cost = data['cost'];
    fruits[indexFruitUpdate].price = data['price'];
    fruits[indexFruitUpdate].status = data['status'];
    if (data['imageFruit'] != null) {
      fruits[indexFruitUpdate].imageFruit = result['imageFruit']['url'];
    }

    return result;
  }

  Future<List<FruitModel>> getAllFruit() async {
    final result = await storeServices.getAllFruit();
    fruits.assignAll(result);
    return result;
  }

  Future<FoodModel> importFood(Map food) async {
    final result = await storeServices.importFood(food);
    foods.add(result);
    return result;
  }

  Future<List<FoodModel>> getAllFood() async {
    final result = await storeServices.getAllFood();
    foods.assignAll(result);
    return result;
  }

  Future<dynamic> updateFood(String objectId, Map data) async {
    final result = await storeServices.updateFood(objectId, data);
    final indexFoodUpdate =
        foods.indexWhere((element) => element.objectId == objectId);
    foods[indexFoodUpdate].cost = data['cost'];
    foods[indexFoodUpdate].price = data['price'];
    foods[indexFoodUpdate].status = data['status'];
    if (data['imageFood'] != null) {
      foods[indexFoodUpdate].imageFood = result['imageFood']['url'];
    }

    return result;
  }

  Future<List<RefundModel>> getRefund() async {
    final result = await storeServices.getRefund();
    refund.value = result[0];
    return result;
  }

  Future<RefundModel> updateRefund(
      String objectId, RefundModel newRefund) async {
    final result = await storeServices.updateRefund(objectId, newRefund);
    refund.value = result;
    return result;
  }
}
