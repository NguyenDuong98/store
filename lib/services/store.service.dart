import 'dart:convert';

import 'package:banhistore/models/food.model.dart';
import 'package:banhistore/models/fruit.model.dart';
import 'package:banhistore/models/product.model.dart';
import 'package:banhistore/models/refund.model.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class StoreServices {
  Future<ProductModel> importProduct(Map product) async {
    final newProduct = ParseObject('Products')
      ..set('productName', product['productName'])
      ..set('cost', product['cost'])
      ..set('price', product['price'])
      ..set('supplier', product['supplier'])
      ..set('amount', product['amount'])
      ..set('imageProduct', product['imageProduct']);
    final response = await newProduct.save();
    return ProductModel.fromJson(
        jsonDecode(jsonEncode(jsonDecode(jsonEncode(response.result)))));
  }

  Future<List<ProductModel>> getAllProduct() async {
    final apiResponse = await ParseObject('Products').getAll();
    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => ProductModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<List<ProductModel>> searchProduct(String keySearch) async {
    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Products'));
    parseQuery.whereContains('productName', '$keySearch');
    final ParseResponse apiResponse = await parseQuery.query();

    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => ProductModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<dynamic> updateProduct(String objectId, Map product) async {
    var updateProduct = ParseObject('Products')
      ..objectId = objectId
      ..set('cost', product['cost'])
      ..set('price', product['price'])
      ..set('amount', product['amount']);
    if (product['imageProduct'] != null) {
      updateProduct..set('imageProduct', product['imageProduct']);
    }
    final response = await updateProduct.save();
    return response.result;
  }

  Future<FruitModel> importFruit(Map fruit) async {
    final newFruit = ParseObject('Fruits')
      ..set('fruitName', fruit['fruitName'])
      ..set('origin', fruit['origin'])
      ..set('cost', fruit['cost'])
      ..set('price', fruit['price'])
      ..set('status', fruit['status'])
      ..set('imageFruit', fruit['imageFruit']);

    final response = await newFruit.save();

    return FruitModel.fromJson(
        jsonDecode(jsonEncode(jsonDecode(jsonEncode(response.result)))));
  }

  Future<List<FruitModel>> searchFruit(String keySearch) async {
    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Fruits'));
    parseQuery.whereContains('fruitName', '$keySearch');
    final ParseResponse apiResponse = await parseQuery.query();

    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => FruitModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<dynamic> updateFruit(String objectId, Map fruit) async {
    var updateFruit = ParseObject('Fruits')
      ..objectId = objectId
      ..set('cost', fruit['cost'])
      ..set('price', fruit['price'])
      ..set('status', fruit['status']);
    if (fruit['imageFruit'] != null) {
      updateFruit..set('imageFruit', fruit['imageFruit']);
    }
    final response = await updateFruit.save();

    return response.result;
  }

  Future<List<FruitModel>> getAllFruit() async {
    final apiResponse = await ParseObject('Fruits').getAll();
    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => FruitModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<FoodModel> importFood(Map food) async {
    final newFood = ParseObject('Foods')
      ..set('nameFood', food['nameFood'])
      ..set('cost', food['cost'])
      ..set('price', food['price'])
      ..set('status', food['status'])
      ..set('imageFood', food['imageFood']);

    final response = await newFood.save();

    return FoodModel.fromJson(
        jsonDecode(jsonEncode(jsonDecode(jsonEncode(response.result)))));
  }

  Future<List<FoodModel>> getAllFood() async {
    final apiResponse = await ParseObject('Foods').getAll();
    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => FoodModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<dynamic> updateFood(String objectId, Map fruit) async {
    var updateFruit = ParseObject('Foods')
      ..objectId = objectId
      ..set('cost', fruit['cost'])
      ..set('price', fruit['price'])
      ..set('status', fruit['status']);
    if (fruit['imageFood'] != null) {
      updateFruit..set('imageFood', fruit['imageFood']);
    }
    final response = await updateFruit.save();

    return response.result;
  }

  Future<List<RefundModel>> getRefund() async {
    final apiResponse = await ParseObject('Refund').getAll();
    if (apiResponse.success && apiResponse.results != null) {
      return apiResponse.results
          .map((item) => RefundModel.fromJson(jsonDecode(jsonEncode(item))))
          .toList();
    }
    return [];
  }

  Future<RefundModel> updateRefund(
      String objectId, RefundModel newRefund) async {
    var updateFruit = ParseObject('Refund')
      ..objectId = objectId
      ..set('grocery', newRefund.grocery)
      ..set('fruit', newRefund.fruit)
      ..set('food', newRefund.food);
    final response = await updateFruit.save();

    return RefundModel.fromJson(jsonDecode(jsonEncode(response.result)));
  }
}
