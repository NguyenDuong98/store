import 'dart:convert';

import 'package:banhistore/models/customer.model.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

class CartServices {
  Future<CustomerModel> searchCustomer(String keySearch) async {
    final QueryBuilder<ParseObject> parseQuery =
        QueryBuilder<ParseObject>(ParseObject('Customers'));
    parseQuery.whereContains('customer', '$keySearch');
    final ParseResponse apiResponse = await parseQuery.query();
    print('Dương');
    print(apiResponse.result);
    if (apiResponse.success && apiResponse.results != null) {
      return CustomerModel.fromJson(
          jsonDecode(jsonEncode(apiResponse.result[0])));
    }
    return CustomerModel();
  }
}
